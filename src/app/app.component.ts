import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MyService } from './services/my.service';
import * as $ from 'jquery';
import { UserModel } from './models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  Allusers:UserModel[];
  id:any = "";
  title = 'backendng';

  constructor(
    private toastr: ToastrService
    , private myService: MyService
    ) {   
      this.myService.Allusers.subscribe(res=>{
        this.Allusers = res;
      })

  } 
  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

  add(){
    var form = new FormData();
    form.append("name", $("#addInputName").val());
    form.append("mobile", $("#addInputMobile").val());

    this.myService.add(form).subscribe(res=> {
      var r: any = res;
      console.log("res", res);      
      this.toastr.success(r.message, "Success");
      this.myService.show('');
    },error=>{
      console.log("error", error);
      error.error.error.forEach(el => {
        this.toastr.error(el, "Error");
        
      });
      
    });
  }

  updateModelShow(id){
    this.Allusers.forEach(el=>{
      if( id == el.id ){
        this.id = el.id;
        $("#updateInputName").prop("value", el.name);
        $("#updateInputMobile").prop("value", el.mobile);
      }
    });
  }

  update(){    
    var form = new FormData();
    form.append("id", this.id);
    form.append("name", $("#updateInputName").val());
    form.append("mobile", $("#updateInputMobile").val());

    this.myService.update(form).subscribe(res=> {
      var r: any = res;
      console.log("res", res);      
      this.toastr.success(r.message, "Success");
      this.myService.show('');
    },error=>{
      console.log("error", error);
      error.error.error.forEach(el => {
        this.toastr.error(el, "Error");
        
      });
      
    });
  }

  search(v){
    this.myService.show(v);
  }

  delete(id){
    this.myService.delete(id).subscribe(res=> {
      var r: any = res;
      console.log("res", res);      
      this.toastr.success(r.message, "Success");
      this.myService.show('');
    },error=>{
      console.log("error", error);
      error.error.error.forEach(el => {
        this.toastr.error(el, "Error");
        
      });
      
    });

  }
  
}
